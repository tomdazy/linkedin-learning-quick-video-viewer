# Linkedin Learning quick video viewer

Script permettant de passer les vidéos de Linkedin Learning, utile si vous avez regardé les vidéos avec un pote ou autre.

# Term and conditions

Ce script est **partagé dans un but éducatif** et non commercial, ne l'utilisez pas de manière abusive ou malhonnête.
This script is shared **for Educational Purposes Only**

# How to use

See the [Wiki page](https://gitlab.com/tomdazy/linkedin-learning-quick-video-viewer/wikis/Wiki) to understand how to use it.

# License

This project is distributed under the Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) license. (https://creativecommons.org/licenses/by-nc/4.0/)
