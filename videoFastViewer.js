/**
 * Set video current time to last second
 * @author Thomas Dazy (contact@thomasdazy.fr)
 * November 2019
 *
 * This project is distributed under the Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) license. (https://creativecommons.org/licenses/by-nc/4.0/)
 *
**/

/**
 *
 * GUIDE:
 * Votre navigateur doit accepter l'injection de code JavaScript pour que ce code fonctionne
 * Pour lancer le script, il faut copier le contenu de ce fichier dans la console de votre navigateur
 * Pour arrêter le script il faut reload la page manuellement ou attendre 1 heure
 *
**/

// ---------------------------------------------------------------------------
// Définition des variables
// ---------------------------------------------------------------------------
// On va tenter de passer les vidéos 1800 fois
var x = 1800;
// Avec un interval de 2 secondes entre chaque action
var interval = 2000;

// ---------------------------------------------------------------------------
// Boucle qui passe les vidéos
// ---------------------------------------------------------------------------
for (var i = 0; i < x; i++) {
    setTimeout(function () {
      var video_small_id = $('.video-js').attr('id');
      var video_big_id = video_small_id + '_html5_api';
      var video_duration = document.getElementById(video_big_id).duration;
      var video = document.getElementById(video_big_id);
      video.currentTime = video_duration - 1;
      console.log('Video passed');
    }, i * interval)
}
